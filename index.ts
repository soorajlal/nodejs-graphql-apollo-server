"use strict"

import express from 'express';
import fs from 'fs';
import cors from 'cors';
import  { ApolloServer} from 'apollo-server-express'
import resolvers from './resolvers/resolvers'

const app = express();
const port = 4000;

const schema = fs.readFileSync('./schema/schema.graphql',{encoding:'utf-8'})

const server = new ApolloServer({
    typeDefs: schema,
    resolvers,
});

server.applyMiddleware({ app, path: '/graphql' });

app.use(cors());

app.get('/', (re, res)=>{
    res.send('Welcome');
});

app.listen(port, (err)=>{
    if (err) throw err;
    console.log(`listening to port ${port}`);
});

