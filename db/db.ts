"use strict"

import { DataStore } from 'notarealdb';

const store = new DataStore('./data');

const students = store.collection('students');
const colleges = store.collection('colleges');

export default {students, colleges};