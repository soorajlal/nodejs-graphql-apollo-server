import db from '../db/db';

const resolvers = {
    Query :{
        test: () => 'Test Success, GraphQL server is up & running !!',
        students: () => {
            return db.students.list();
        },
        student:(root,args,context,info) => {
            //args will contain parameter passed in query
            return db.students.get(args.id);
        },
        colleges: () => db.colleges.list(),
        college:(root,args,context,info) => {
            //args will contain parameter passed in query
            return db.colleges.get(args.id);
        }
    },
    Student : {
        fullName:(root,args,context,info) => {
           return root.firstName+":"+root.lastName
        },
        college:(root) => {
           return db.colleges.get(root.collegeId);
        }
    },
    College : {
        students:(root,args,context,info) => {
            let students:Array<any> = db.students.list();
            students  = students.filter((stud)=>{
                return stud.collegeId == root.id;
            });
            return students;
        }
    },
    Mutation: {
        createStudent: (root,args,context,info) => {
            let students = db.students.list();
            //console.log(args);
            const {email,firstName,lastName, collegeId} = args;

            const emailExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            const isValidEmail =  emailExpression.test(String(email).toLowerCase())
            if(!isValidEmail)
                throw new Error("email not in proper format")

            if(firstName == '')
                throw new Error("firstName is required")

            if(lastName == '')
                throw new Error("lastName is required")

            if(collegeId == '')
                throw new Error("collegeId is required")

            //get last id
            let studentArr = students.map(student => student.id);
            let newId = studentArr[studentArr.length - 1]+1;

            let student = {
                id: newId,
                email: args.email,
                firstName: args.firstName,
                lastName: args.lastName,
                collegeId: args.collegeId
            };
            //console.log(student);
            db.students.create(student);
            return db.students.get(newId);

        }
    }
}

 export default resolvers;